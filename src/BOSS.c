#include "stm32l476xx.h"
#include "timer_config.h"
#include "driver_config.h"
#include "BOSS.h"
#include "control.h"

// ADC Conversion
void SysTick_Handler(void)
{
	ADC1->CR |= ADC_CR_ADSTART;												// Start ADC Conversion
	while (!(ADC123_COMMON->CSR & ADC_CSR_EOC_MST));	// Wait for Conversion to finish
	int ADC_Val = ADC1->DR;														// Save ADC Value
	//DisplayAxisValues();
}