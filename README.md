# BOSS #

BOSS (Bisection Optimization for Small Satellites) is a research project conducted by Utah State University's Get Away Special (GAS) Team with funding from the Space Dynamics Laboratory, a UARC under the Missile Defense Agency.

### Overview ###

BOSS is an element of an ADCS (Attitude Determination Control System) on a small satellite that eliminates the need for a feedback controller on reaction wheel speed. BOSS determines what change in electrical current corresponds to a specific control torque before attitude control is needed. 

### Experiment ###

There are three phases to the experiment:

1. Implement BOSS to create a Look-Up Table (LUT) that correlates a change in electrical current to a desired control torque

2. Using the desired control torque and angular acceleration measurements, determine the Small Satellite's Moment of Inertia (MOI) about the Z axis 

3. Implement a Proportional-Integral-Derivate (PID) controller that minimizes the error between the desired angular orientation and the current angular orientation of the Small Satellite (a plastic box in this project)
    * The "Small Satellite" will have a bright light shining on its external photodiode. The electrical signal from the diode will be recorded as the "desired orientation". The Small Satellite will then be turned away from the light and will try to reach its initial orientation using the light's brightness as a metric.

### System Diagram ###
Place a diagram here.

### Researchers ###

* Ryan Berg, Principal Investigator
* Chaz Cornwall, Algorithm Development