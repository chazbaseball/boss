#ifndef DRIVER_CONFIG_H
#define DRIVER_CONFIG_H

#define MAX_BUFFER_SIZE 2000
#define BASE_10 				10
#define ACCELEROMETER_READ 0x3A
#define ACCELEROMETER_WRITE 0x3B
#define ENABLE 1
#define DISABLE 0
#define GYRO_BUFFER_SIZE 95
#define GYRO_SAMPLE_RATE 95

void LED_Config();
void Red_LED_On(unsigned char state);
void Green_LED_On(unsigned char state);
void gyro_Config();
float calculateAcceleration();
void WriteToGyro(unsigned char gyroRegister, unsigned char value);
unsigned char ReadFromGyro(unsigned char gyroRegister);
void WaitForSPI1TXReady();
void WaitForSPI1RXReady();
short GetAxisValue(unsigned char lowRegister, unsigned char highRegister);
void GetGyroValues(short* x, short* y, short* z);
void InitializeGyro();
void getGyroValue(unsigned char bufferIncrement);
void delay(unsigned int delay);

void AccelerometerInit();
void GetAccelerometerValues(short *x, short *y, short *z);
void WriteToAccelerometer(unsigned short registerAddress, unsigned char data);
unsigned char ReadFromAccelerometer(unsigned short registerAddress);
unsigned char I2CGetData();
void I2CWaitIfBusy();
void I2CSendRegister(unsigned short registerAddress);
void I2CSendSlaveAddress(unsigned short address);
void I2CEnableAcknowledge();
void I2CStop();
void I2CStartRestart();
void I2C_Start(char direction, unsigned int slaveAddress);

void ADC_Config();
void ADC_wakeup();

void Motor_PWM_config();
void update_duty_cycle(float old_duty_cycle, char direction, char bisection_flag);

void DMA1_Channel7_IRQHandler(void);
void USART2_IRQHandler(void);
void UartGpioInit();
void UartDmaInit();
void UartInit();
void SendString(char* string);
char* IntegerToString(int value, char *result, int base);

void DisplayHeader();
void DisplayIntegerValue(short value);
void DisplayAxisValue(char* label, short accel, short gyro);
void DisplayAxisValues();

#endif //DRIVER_CONFIG_H