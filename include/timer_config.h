#ifndef TIMER_CONFIG_H
#define TIMER_CONFIG_H

void Red_LED_Flash(unsigned char state);
void Green_LED_Flash(unsigned char state);

void HSI_Clock_Enable();	// Enable HSI
void Sys_Tick_Conf();			// ADC Driver Clock
void timer1_config();			// I2C (MEMS) Clock
void timer2_config();		  // Gyro Interrupt
void timer3_config();			// Acceleration calculation Timer
void timer4_config();			// LED Flash Interrupt
void timer5_config();			// Motor PWM Clock

void TIM2_IRQHandler(void);
void TIM4_IRQHandler(void);

#endif //TIMER_CONFIG_H