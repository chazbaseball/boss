#ifndef UART_H
#define UART_H

#define MAX_BUFFER_SIZE 2000
#define BASE_10 				10

volatile int uartBusy;

void DMA1_Channel7_IRQHandler(void);
void USART2_IRQHandler(void);
void UartGpioInit();
void UartDmaInit();
void UartInit();
void SendString(char* string);
char* IntegerToString(int value, char *result, int base);

void DisplayHeader();
void DisplayIntegerValue(short value);
void DisplayAxisValue(char* label, short accel, short gyro);
void DisplayAxisValues();

#endif //UART_H