#include "stm32l476xx.h"
#include "timer_config.h"
#include "driver_config.h"
#include "BOSS.h"
#include "control.h"

unsigned char GLEDF;
unsigned char RLEDF;
unsigned char gyroBufferIncrement = 0;

void Red_LED_Flash(unsigned char state)
{
	if (state == DISABLE)
	{
		GPIOB->ODR &= ~GPIO_ODR_OD2;
		RLEDF = 0;
	}
	else
		RLEDF = 1;
}
void Green_LED_Flash(unsigned char state)
{
	if (state == DISABLE)
	{
		GPIOE->ODR &= ~GPIO_ODR_OD8;
		GLEDF = 0;
	}
	else
	{
		GLEDF = 1;
	}
}

// Switch system clock to HSI
void HSI_Clock_Enable()
{
	RCC->CR |= RCC_CR_HSION; 											// HSI clock on
	while((RCC->CR & RCC_CR_HSIRDY) == 0);				// Wait for clock to enable
}

// ADC Driver Clock
void Sys_Tick_Conf()
{
	const uint32_t ticks = 1600;									// ADC Conversion Rate = 10 kHz
	SysTick->CTRL = 0;														// Reset Sys_Tick
	SysTick->LOAD = ticks - 1;										// Load Restart Value
	SysTick->VAL = 0;															// Reset Counter
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;	// Select System Clock (HSI)
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;		// Enable Interrupts
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;			// Enable Sys_Tick
	RCC->AHB2ENR |= RCC_APB2ENR_SYSCFGEN;					// Enable Counter
}

// Gyro Sampling Clock
void timer2_config()
{
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;					// Enable GPIOA Clock
	
	GPIOA->MODER &= ~GPIO_MODER_MODE5;						// Configure PA5 for alternate function mode
	GPIOA->MODER |= GPIO_MODER_MODE5_1;
	
	GPIOA->AFR[0] |= (1 << 20);
	
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED5;			// Low I/O Speed
	
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPD5;						// No Pull up or Pull Down
	
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;					// Enable Timer 2 Clock
	
	TIM2->CR1 &= ~TIM_CR1_CEN;										// Disable Timer 2
	TIM2->CR1 &= ~TIM_CR1_DIR;										// Select up-counting
	TIM2->SR &= ~TIM2->SR;												// Clear the Status Register
	TIM2->CNT = 0;																// Clear Counter
	TIM2->PSC = 15999;														// Timer 2 frequency = 1 kHz
	TIM2->ARR = 999;															// 1 Count per clock period
	TIM2->CCR1 = 0;															  // Output capture (interrupt) every second
	TIM2->BDTR |= TIM_BDTR_MOE;										// Main output of timer 2 enabled
	TIM2->CCMR1 &= ~TIM_CCMR1_OC1M;								// Select Active Mode
	TIM2->CCER |= TIM_CCER_CC1E;
	TIM2->CR2 |= (4 << 4);
	TIM2->DIER |= TIM_DIER_CC1IE;										// Enable Interrupts
	NVIC_SetPriority(TIM2_IRQn, 3);
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->CR1 |= TIM_CR1_CEN;											// Enable Timer 2
}

void timer3_config()
{
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;					// Enable GPIOA Clock
	
	GPIOA->MODER &= ~GPIO_MODER_MODE3;						// Configure PA5 for alternate function mode
	GPIOA->MODER |= GPIO_MODER_MODE3_1;
	
	GPIOA->AFR[0] |= (1 << 12);
	
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED3;			// Low I/O Speed
	
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPD3;						// No Pull up or Pull Down
	
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM3EN;					// Enable Timer 2 Clock
	
	TIM3->CR1 &= ~TIM_CR1_CEN;										// Disable Timer 2
	TIM3->CR1 &= ~TIM_CR1_DIR;										// Select up-counting
	TIM3->SR &= ~TIM3->SR;												// Clear the Status Register
	TIM3->CNT = 0;																// Clear Counter
	TIM3->PSC = 15999;														// Timer 2 frequency = 1 kHz
	TIM3->ARR = 999;															// 1 Count per clock period
	TIM3->CCR1 = 0;															  // Output capture (interrupt) every second
	TIM3->BDTR |= TIM_BDTR_MOE;										// Main output of timer 2 enabled
	TIM3->CCMR1 &= ~TIM_CCMR1_OC1M;								// Select Active Mode
	TIM3->CCER |= TIM_CCER_CC1E;
	TIM3->CR2 |= (4 << 4);
	TIM3->DIER |= TIM_DIER_CC1IE;									// Enable Interrupts
	NVIC_SetPriority(TIM3_IRQn, 3);
	NVIC_EnableIRQ(TIM3_IRQn);
	TIM3->CR1 |= TIM_CR1_CEN;											// Enable Timer 3
}

// LED Interrupt Timer
void timer4_config()
{
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;					// Enable GPIOA Clock
	
	GPIOB->MODER &= ~GPIO_MODER_MODE6;						// Configure PA5 for alternate function mode
	GPIOB->MODER |= GPIO_MODER_MODE6_1;
	
	GPIOA->AFR[0] |= (1 << 24);
	
	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED6;			// Low I/O Speed
	
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPD6;						// No Pull up or Pull Down
	
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM4EN;					// Enable Timer 2 Clock
	
	TIM4->CR1 &= ~TIM_CR1_CEN;										// Disable Timer 2
	TIM4->CR1 &= ~TIM_CR1_DIR;										// Select up-counting
	TIM4->SR &= ~TIM2->SR;												// Clear the Status Register
	TIM4->CNT = 0;																// Clear Counter
	TIM4->PSC = 1599;															// Timer 2 frequency = 10 kHz
	TIM4->ARR = 999;															// 1 Count per clock period
	TIM4->CCR1 = 500;															// Output capture (interrupt) every second
	TIM4->BDTR |= TIM_BDTR_MOE;										// Main output of timer 2 enabled
	TIM4->CCMR1 &= ~TIM_CCMR1_OC1M;								// Select Active Mode
	TIM4->CCER |= TIM_CCER_CC1E;
	TIM4->CR2 |= (4 << 4);
	TIM4->DIER |= TIM_DIER_CC1IE;									// Enable Interrupts
	NVIC_SetPriority(TIM4_IRQn, 3);
	NVIC_EnableIRQ(TIM4_IRQn);
	GLEDF = 0;
	RLEDF = 0;
	TIM4->CR1 |= TIM_CR1_CEN;											// Enable Timer4
}

// Gyro Sampling Clock
void TIM2_IRQHandler(void) // Use whenever we want to get a sample from the Gyro
{
	if ((TIM2->SR & TIM_SR_CC1IF) != 0)
	{
		gyroBufferIncrement++;
		DisplayAxisValues();
		getGyroValue(gyroBufferIncrement);
		if (gyroBufferIncrement == 94)
		{
			calculateAcceleration();
			gyroBufferIncrement = 0;
		}
		TIM2->SR &= ~TIM_SR_CC1IF;
	}
}

// LED Interrupt Timer
void TIM4_IRQHandler(void) 
{
	if ((TIM4->SR & TIM_SR_CC1IF) != 0)
	{
		if (RLEDF == 1)
		{
			if ((GPIOB->ODR & GPIO_ODR_OD2) == 0)
				GPIOB->ODR |= GPIO_ODR_OD2;
			else
				GPIOB->ODR &= ~GPIO_ODR_OD2;
		}
		if (GLEDF == 1)
		{
			if ((GPIOE->ODR & (1 << 8)) > 1)
				GPIOE->ODR &= ~GPIO_ODR_OD8;
			else
				GPIOE->ODR |= GPIO_ODR_OD8;
		}
		TIM4->SR &= ~TIM_SR_CC1IF;
	}
}

// I2C (MEMS) clock driven to PD1
void timer1_config()
{
	
}

// Motor PWM clock
void timer5_config()
{
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM5EN;		// Enable Timer 5 Clock
	TIM5->CR1 	  &= ~TIM_CR1_DIR;					// Select Up Counting
	TIM5->PSC 	  = 15;											// Clock Frequency = 1000 kHz (4 MHz/(3 + 1)
	TIM5->ARR 	  = 999;										// PWM period = (999 + 1) * 1/100kHz = 0.001 second
	TIM5->CCMR1  &= ~TIM_CCMR1_OC1M;				// Clear Output Compare bits for channel 1
	TIM5->CCMR1  |= TIM_CCMR1_OC1M_1;				// Select PWM Mode 1 output on channel 1 (OC1M = 110)
	TIM5->CCMR1  |= TIM_CCMR1_OC1M_2;
	TIM5->CCMR1  |= TIM_CCMR1_OC1PE;				// Enable channel 1 preload
	TIM5->CCER   &= ~TIM_CCER_CC1P;					// Active high output
  TIM5->CCER   |= TIM_CCER_CC1E;					// Enable main output to PA0
	TIM5->BDTR   |= TIM_BDTR_MOE;						// Main output enable to PA0
	TIM5->CCR1   |= 0;											// Initial Duty Cycle of 0%;
	TIM5->CR1    |= TIM_CR1_CEN;						// Enable Counter
}