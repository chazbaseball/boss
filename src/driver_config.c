#include "stm32l476xx.h"
#include "timer_config.h"
#include "driver_config.h"
#include "BOSS.h"
#include "control.h"
#include "UART.h"

volatile int uartBusy;
unsigned char dmaBuffer[MAX_BUFFER_SIZE];
unsigned char gyroBuffer[GYRO_BUFFER_SIZE];

// Use LEDs to indicate progress in the BOSS control Algorithm
// - Red Flashing: 		BOSS Has Started
// - Red Solid: 	 		BOSS is Finished
// - Green Flashing: 	Light Pointing has Started
// - Green Solid: 		Pointing is Finished
void LED_Config()
{
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
	
	// Configure PB2 for Output Mode Push-Pull, Pull Down
	GPIOB->MODER &= ~GPIO_MODER_MODE2;
	GPIOB->MODER |= GPIO_MODER_MODE2_0;
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT2;
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD2;
	GPIOB->PUPDR |= GPIO_PUPDR_PUPD2_1;
	
	// Configure PE8 for Output Mode Push-Pull, Pull Down
	GPIOE->MODER &= ~GPIO_MODER_MODE8;
	GPIOE->MODER |= GPIO_MODER_MODE8_0;
	GPIOE->OTYPER &= ~GPIO_OTYPER_OT8;
	GPIOE->PUPDR &= ~GPIO_PUPDR_PUPD8;
	GPIOE->PUPDR |= GPIO_PUPDR_PUPD8_1;
}

void Red_LED_On(unsigned char state)
{
	if (state == DISABLE)
	{
		Red_LED_Flash(DISABLE);
		GPIOB->ODR	&= ~GPIO_ODR_OD2;
	}
	else // state = ENABLE
	{
		Red_LED_Flash(DISABLE);
		GPIOB->ODR |= GPIO_ODR_OD2;
	}
}
void Green_LED_On(unsigned char state)
{
	if (state == DISABLE)
	{
		Green_LED_Flash(DISABLE);
		GPIOE->ODR	&= ~GPIO_ODR_OD8;
	}
	else // state = ENABLE
	{
		Green_LED_Flash(DISABLE);
		GPIOE->ODR |= GPIO_ODR_OD8;
	}
}
// See page 25 of the L3GD20 datasheet for details on how we send commands to, and make
// queries of, the gyro. 
//    CS  --> PD7   // Chip Select
//    SPC --> PD1   // Clock
//    SDI --> PD4   // Data In
//    SDO --> PD3   // Data Out
// As shown on page 25 of the L3GD20 communication with the gyro takes the following steps:
// 1) The chip select must go from high to low to indicate we're doing a read or write.
// 2) The first byte we send is the register of the gyro (combined with 0x80 if we're going
//    to do a read operation).
// 3) Do a read from SDO
// 4) Write the next byte
// 5) Read a result
// 6) Set CS back to high
void gyro_Config()
{
	// Enable GPIOD Clock
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
	
	// Configure PD1, PD2, PD3, PD4, and PD7 for Alternate Function Mode
	GPIOD->MODER &= ~GPIO_MODER_MODE0;
	GPIOD->MODER &= ~GPIO_MODER_MODE1;
	GPIOD->MODER &= ~GPIO_MODER_MODE2;
	GPIOD->MODER &= ~GPIO_MODER_MODE3;
	GPIOD->MODER &= ~GPIO_MODER_MODE4;
	GPIOD->MODER &= ~GPIO_MODER_MODE7;
	GPIOD->MODER |= (1|(2 << 2)|(2 << 6)|(2 << 8)|(1 << 14));
	
	// Configure PD1, PD2, PD3, PD4, and PD7 for SPI Clock (AF5)
	//GPIOD->AFR[0] &= ~GPIOD->AFR[0];
	GPIOD->AFR[0] |= ((5 << 4)|(5 << 12)|(5 << 16));
	
	// Configure SPI pins for Fast Speed
	//GPIOD->OSPEEDR &= ~GPIOD->OSPEEDR;
	GPIOD->OSPEEDR |= (3|(3 << 2)|(3 << 6)|(3 << 8)|(3 << 14));
	
	// Enable SPI2 Clock
	RCC->APB1ENR1 |= RCC_APB1ENR1_SPI2EN;
	
	// Set Bit 0: The second clock transition is the first data capture edge
	// Set Bit 1: CK to 1 when idle
	// Set Bit 2: The STM32 is the master, the gyro is the slave
	// Set Bits 3-5 to 010 for a baud rate of fPCLK/32
	// Set Bits 8-9: Software slave management enabled, Internal slave select to 1
	SPI2->CR1 |= (1 | (1 << 1) | (1 << 2) | (4 << 3) | (1 << 8) | (1 << 9));
	
	// Set Bit 6: Enable SPI.  See page 603.
	SPI2->CR1 |= (1 << 6);

	// Set the CS high on the gyro as setting it low indicates communication.  See
	// 25 of the L3GD20 datasheet and page 159 of RM0383 for more info on BSRR.
	GPIOD->BSRR |= (1|(1 << 7));

	// See page 31 of the L3GD20 datasheet.  If communication to the gyro is properly
	// setup, reading from register 0x0F will give us b11010100 (0xD4).
	if(ReadFromGyro(0x0F) != 0xD4)
	{
		while(1);
	}

	// See page 31 of the L3GD20 datasheet.  Writing 0x0F to register 0x20 will power up
	// the gyro and enables the X, Y, Z axes.
	WriteToGyro(0x20, 0x0F);
	int ctlReg = ReadFromGyro(0x20);
	ctlReg++;
}

float calculateAcceleration()
{
	unsigned char numSamples = GYRO_BUFFER_SIZE - 1;
	float accelValue[numSamples], averageAccel;
	for (unsigned int i = 0; i < numSamples; i++)
	{
		accelValue[i] = ((float)gyroBuffer[i] - (float)gyroBuffer[i+1]) * (1/GYRO_SAMPLE_RATE);
		averageAccel += accelValue[i];
	}
	averageAccel = averageAccel / (float)numSamples;
}

void WaitForSpi2RXReady()
{
	// See page 605 of the datasheet for info on the SPI status register
	// If Bit0 == 0 then SPI RX is empty
	// If Bit7 == 1 then SPI is busy
	while (((SPI2->SR & 1) == 0) || ((SPI2->SR & (1 << 7)) == 1));
}

void WaitForSpi2TXReady()
{
	// See page 605 of the datasheet for info on the SPI status register
	// If Bit1 == 0 then SPI TX buffer is not empty
	// If Bit7 == 1 then SPI is busy
	while (((SPI2->SR & (1 << 1)) == 0) || ((SPI2->SR & (1 << 7)) == 1));
}

unsigned char ReadFromGyro(unsigned char gyroRegister)
{
	GPIOD->BSRR |= ((1 << 16)|(1 << 23));
	WaitForSpi2TXReady();
	SPI2->DR = (gyroRegister | 0x80); // 0x80 indicates we're doing a read
	WaitForSpi2RXReady();
	SPI2->DR;  // I believe we need this simply because a read must follow a write
	WaitForSpi2TXReady();
	SPI2->DR = 0xFF;
	WaitForSpi2RXReady();
	volatile unsigned char readValue = (unsigned char)SPI2->DR;
	GPIOD->BSRR |= (1|(1 << 7));

	return readValue;
}

void WriteToGyro(unsigned char gyroRegister, unsigned char value)
{
	GPIOD->BSRR |= ((1 << 16)|(1 << 23));
	WaitForSpi2TXReady();
	SPI2->DR = gyroRegister;
	WaitForSpi2RXReady();
	SPI2->DR;  // I believe we need this simply because a read must follow a write
	WaitForSpi2TXReady();
	SPI2->DR = value;
	WaitForSpi2RXReady();
	SPI2->DR;  // Don't care what value the device put into the data register
	GPIOD->BSRR |= (1|(1 << 7));
}

short GetAxisValue(unsigned char lowRegister, unsigned char highRegister)
{
	// See page 9 of L3GD20.  It shows the mechanical characteristics of the gyro.  Note
	// that we leave the sensitivity as is (i.e. 0) so that it's 250 dps.  So, we read
	// the value from the gyro and convert it to a +/- 360 degree value.
	float scaler = 8.75; /* *0.001;*/
	short temp = (ReadFromGyro(lowRegister) | (ReadFromGyro(highRegister) << 8));
	return (short)((float)temp * scaler);
}

void GetGyroValues(short* x, short* y, short* z)
{
	// See page 36 of the L3GD20 datasheet.  It shows the hi/lo addresses of each of the axes.
	while((ReadFromGyro(0x26) & (1 << 3)) == 0);
	//unsigned char status_reg = ReadFromGyro(0x26);
	*x = GetAxisValue(0x28, 0x29);
	*y = GetAxisValue(0x2A, 0x2B);
	*z = GetAxisValue(0x2C, 0x2D);
}

void InitializeGyro()
{
	SendString("Initializing Gyroscope\r\n");
	gyro_Config();
}

void getGyroValue(unsigned char bufferIncrement)
{
	
}

void delay(unsigned int delay)
{
	for(volatile int i = 0; i < delay; i++);
}

void I2CStartRestart()
{
	// Send a start condition to initiate master mode
	I2C1->CR1 |= (1 << 8);
}

void I2CStop()
{
	I2C1->CR2 |= I2C_CR2_STOP;								// Set stop bit
	while((I2C1->ISR & I2C_ISR_STOPF) == 0);	// Wait until stopf flag is reset
	I2C1->ICR |= I2C_ICR_STOPCF;							// Clear stopf flag
}

void I2CEnableAcknowledge()
{
	// Turn acknowledge on here.
	I2C1->CR2 |= (1 << 15);
}

void I2CDisableAcknowledge()
{
	// Turn acknowledge off here.
	I2C1->CR2 &= ~(1 << 15);
}

void I2CSendSlaveAddress(unsigned short address)
{
	// Write the address into the I2C data register.
	I2C1->TXDR = address;
}

void I2CSendRegister(unsigned short registerAddress)
{
	// Put the register address into the data register
  I2C1->TXDR = registerAddress;
}

void I2CWaitIfBusy()
{
	// See page 502 of the datasheet.  Bit 15 of ISR will be set when the I2C bus is busy.
	while((I2C1->ISR & (1 << 15)) == (1 << 15));
}

void I2CWriteByte(unsigned char data)
{
	// See page 500 of the datasheet.  Bit 0 will be reset when the data transfer is ready.
	while((I2C1->ISR & 1) == 1);
	
	// Write the data to the data register
	I2C1->TXDR = data;
}

unsigned char I2CGetData()
{
	// Bit 2 of ISR will be set when receiver data is finished sending
	while((I2C1->ISR & (1 << 2)) == 0);

	// Return the data
	return I2C1->RXDR;
}

void I2C_Start(char direction, unsigned int slaveAddress)
{
	uint32_t tmpreg = I2C1->CR2;
	tmpreg &= (uint32_t)~((uint32_t)(I2C_CR2_SADD | I2C_CR2_NBYTES |
																	 I2C_CR2_RELOAD | I2C_CR2_AUTOEND |
																	 I2C_CR2_RD_WRN | I2C_CR2_START |
																	 I2C_CR2_STOP));
	if (direction == 0) // Read from slave
	{
		tmpreg |= I2C_CR2_RD_WRN;
	}
	else								// Write to slave
	{
		tmpreg &= ~I2C_CR2_RD_WRN;
	}
	
	tmpreg |= (uint32_t)((((uint32_t)slaveAddress << 1) & I2C_CR2_SADD) |
											(((uint32_t)1 << 16) & I2C_CR2_NBYTES));
	
	tmpreg |= I2C_CR2_START;
	I2C1->CR2 = tmpreg;
}

unsigned char ReadFromAccelerometer(unsigned short registerAddress)
{
	I2CWaitIfBusy();

	I2C_Start(0, ACCELEROMETER_READ);

	char data;
	while((I2C1->ISR & I2C_ISR_RXNE) == 0);
	data = I2C1->RXDR & I2C_RXDR_RXDATA;
	I2CStop();
	return data;
}

void WriteToAccelerometer(unsigned short registerAddress, unsigned char data)
{
	I2CWaitIfBusy();

	I2C_Start(1, ACCELEROMETER_WRITE);
	while ((I2C1->ISR & I2C_ISR_TXIS) == 0);
	I2C1->TXDR = data & I2C_TXDR_TXDATA;
	while ((I2C1->ISR & I2C_ISR_TC) == 0 && (I2C1->ISR & I2C_ISR_NACKF) == 0);
	if ((I2C1->ISR & I2C_ISR_NACKF) != 0)
		while(1);
	I2CStop();
}

void AccelerometerInit()
{
	// Give a clock to port B as pins PB6 and PB7 are connected to the accelerometer.
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;

	// See page 156 of the datasheet.  We configure PB6 and PB7 to alternate function.
	GPIOB->MODER &= ~GPIO_MODER_MODE6;
	GPIOB->MODER &= ~GPIO_MODER_MODE7;
	GPIOB->MODER |= GPIO_MODER_MODE6_1 | GPIO_MODER_MODE7_1;

	// Configure to AF4 for I2C1
	GPIOB->AFR[0] |= ((4 << 24)|(4 << 28));
	
	// Set the pins to very fast speed
	GPIOB->OSPEEDR |= ((3 << 12) | (3 << 14));
	
	// Configure PB6 and PB7 for open drain
	GPIOB->OTYPER |= GPIO_OTYPER_OT6;
	GPIOB->OTYPER |= GPIO_OTYPER_OT7;
	
	// Configure PB6 and PB7 for Pull Up
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD6;
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD7;
	GPIOB->PUPDR |= GPIO_PUPDR_PUPD6_0;
	GPIOB->PUPDR |= GPIO_PUPDR_PUPD7_0;

	// Give a clock to I2C1 by setting bit 21 of the RCC APB1 peripheral clock enable register.
	RCC->APB1ENR1 |= RCC_APB1ENR1_I2C1EN;
	RCC->CCIPR &= ~RCC_CCIPR_I2C1SEL;
	RCC->CCIPR |= RCC_CCIPR_I2C1SEL_1;
	RCC->APB1RSTR1 |= RCC_APB1RSTR1_I2C1RST;
	RCC->APB1RSTR1 &= ~RCC_APB1RSTR1_I2C1RST;

	I2C1->CR1 &= ~I2C_CR1_PE;
	I2C1->CR1 &= ~I2C_CR1_ANFOFF;
	I2C1->CR1 &= ~I2C_CR1_DNF;
	I2C1->CR1 |= I2C_CR1_ERRIE;
	I2C1->CR1 &= ~I2C_CR1_NOSTRETCH;

	I2C1->TIMINGR &= ~I2C_TIMINGR_PRESC;
	I2C1->TIMINGR |= (3U << 28);
	I2C1->TIMINGR |= 0x13U;					// SCLL = 0x13  (5.0 uSeconds)
	I2C1->TIMINGR |= (0xFU << 8);		// SCLH = 0xF   (4.0 uSeconds)
	I2C1->TIMINGR |= (0x1U << 16);	// SCLDEL = 0x4 (0.5 uSeconds)
	I2C1->TIMINGR |= (0x1U << 20);	// SDADEL = 0x4 (0.5 uSeconds)

	I2C1->OAR1 &= ~I2C_OAR1_OA1EN;
	I2C1->OAR2 &= ~I2C_OAR2_OA2EN;
	
	// CR2 Configuration
	I2C1->CR2 &= ~I2C_CR2_ADD10;			// 7-bit addressing mode
	I2C1->CR2 |= I2C_CR2_AUTOEND;			// Enable the auto end

	// Enable I2C1.  See page 494 of the datasheet.
	I2C1->CR1 |= I2C_CR1_PE;

	// Who am I?
	unsigned char accelerometerID = ReadFromAccelerometer(0xF);
	if(accelerometerID != 0x21)
	{
		while(1);
	}

	// See page 25 of the LSM303C datasheet.  Register 0x20 is CTRL_REG1_A.  We set this
	// register to 0x37 (0011 0111) to configure the accelerometer as follows:
	// 1) Normal / low-power mode (100 Hz)
	// 2) Enable X, Y and Z axes
	WriteToAccelerometer(0x20, 0x37);
}

void GetAccelerometerValues(short* x, short* y, short* z)
{
	*x = ((ReadFromAccelerometer(0x29) << 8) | ReadFromAccelerometer(0x28));
	*y = ((ReadFromAccelerometer(0x2B) << 8) | ReadFromAccelerometer(0x2A));
	*z = ((ReadFromAccelerometer(0x2D) << 8) | ReadFromAccelerometer(0x2C));
}

void ADC_Config()
{
	//Configure pin PB2 as output to drive red LED
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
	
	//Configure PA1 as analog mode for input to the ADC
	GPIOA->MODER |= 3U<<4;
	GPIOA->PUPDR &= ~(3U<<4);
	GPIOA->ASCR |= 1U<<2;
	
	// Enable ADC clock bit
	RCC->AHB2ENR |= RCC_AHB2ENR_ADCEN;
	
	// Disable ADC1
	ADC1->CR &= ~(ADC_CR_ADEN);
	
	// Enable I/O analog switches voltage booster
	SYSCFG->CFGR1 |= SYSCFG_CFGR1_BOOSTEN;
	
	// Enable internal channel conversion
	ADC123_COMMON->CCR |= ADC_CCR_VREFEN;
	
	// Configure the ADC prescaler
	ADC123_COMMON->CCR &= (uint32_t)(0xFFC3FFFFU);
	
	// Configure ADC clock mode
	ADC123_COMMON->CCR |= ADC_CCR_CKMODE_0;
	
	// Clear ADC clock register dual bits
	ADC123_COMMON->CCR &= ~(ADC_CCR_DUAL);
	
	// Good Morning :)
	ADC_wakeup();
	
	// Set resolution to 12 bits
	ADC1->CFGR &= ~(ADC_CFGR_RES); 
	
	// Select Right Alignment
	ADC1->CFGR &= ~(ADC_CFGR_ALIGN);
	
	// Select regular channel conversion sequence
	ADC1->SQR1 &= ~(ADC_SQR1_L);
	
	// Specify the channel number 7 as the first conversion in the regular sequence
	ADC1->SQR1 |= ADC_SQR1_SQ1_1;
	ADC1->SQR1 |= ADC_SQR1_SQ1_2;
	ADC1->SQR1 |= ADC_SQR1_SQ1_3;
	
	// Configure the channel 7 as single ended
	ADC1->DIFSEL &= ~(ADC_DIFSEL_DIFSEL_7);
	
	// Set the sample rate
	ADC1->SMPR1 |= ADC_SMPR1_SMP7_0;
	ADC1->SMPR1 |= ADC_SMPR1_SMP7_1;
	ADC1->SMPR1 |= ADC_SMPR1_SMP7_2;
	
	// Select Discontinuous Mode
	ADC1->CFGR &= ~(ADC_CFGR_CONT);
	
	//Select Software Trigger
	ADC1->CFGR &= ~(ADC_CFGR_EXTEN);
	
	// Enable ADC1
	ADC1->CR |= ADC_CR_ADEN;
	
	while((ADC1->ISR & ADC_ISR_ADRDY) == 0);
}

void ADC_wakeup()
{
	int wait_time;
	
	if ((ADC1->CR & ADC_CR_DEEPPWD) == ADC_CR_DEEPPWD)
	{
		ADC1->CR &= ~ADC_CR_DEEPPWD;
	}
	ADC1->CR |= ADC_CR_ADVREGEN;
	
	wait_time = 20 * (80000000 / 1000000);
	while (wait_time != 0)
	{
		wait_time--;
	}
}

void Motor_PWM_config()
{
	RCC->AHB2ENR  |= RCC_AHB2ENR_GPIOAEN;		// Enable GPIOA Clock
	GPIOA->MODER  |= ~GPIOA->MODER;					// Reset all GPIO pins
	GPIOA->MODER  &= ~GPIO_MODER_MODE0;			// Set PA0 to AF mode
	GPIOA->MODER  |= GPIO_MODER_MODE0_1;
	GPIOA->MODER  &= ~GPIO_MODER_MODE1;			// Set PA1 to output mode
	GPIOA->MODER  |= GPIO_MODER_MODE1_1;
	
	GPIOA->OTYPER &= ~GPIOA->OTYPER;				// Set all GPIOA outputs to push-pull
	GPIOA->PUPDR  &= GPIOA->PUPDR;					// Set all GPIOA inputs to no pull up or pull down
	
	GPIOA->ODR    &= ~GPIOA->ODR;						// Clear the ODR
	GPIOA->ODR    |= GPIO_ODR_OD1;					// Set PA1 high
	
	GPIOA->AFR[0] &= ~GPIOA->AFR[0];				// CLear the AFRL register
	GPIOA->AFR[0] |= GPIO_AFRL_AFSEL0_1;		// Set PA0 output to Timer 5 channel 1
}
void update_duty_cycle(float old_duty_cycle, char direction, char bisection_flag)
{
	if (direction == 1)
	{
		
	}
	else
	{
		
	}
}

unsigned char dmaBuffer[MAX_BUFFER_SIZE];

void DMA1_Channel7_IRQHandler(void)
{
	// See pg 187.  Bit 21 is the "Stream 7 transfer complete interrupt flag".  This bit
	// will be set when the DMA transfer is complete.
	if(((DMA1->ISR) & (1 << 25)) != 0)
	{
		// See pg 188.  Here we clear the transfer complete interrupt.
		DMA1->IFCR |= (1 << 25);

		// See pg 552.  Here we specify that we want an interrupt generated once the
		// USART transmission is complete.
		USART2->CR1 |= (1 << 6);
	}
}

void USART2_IRQHandler(void)
{
	// See pg 549.  Bit 6 of the status register will be set when the UART
	// transmission has completed.
	if(((USART2->ISR) & (1 << 6)) != 0)
	{
		// Clear the interrupt. (...So that it doesn't continually trigger)
		//USART2->CR1 &= ~(1 << 6);
		
		// Clear the transfer complete flag in the UART SR
	  USART2->ICR |= (1 << 6);

		// Clear the busy flag to allow the next transmission.
		//uartBusy = 0;
	}
}

void UartGpioInit()
{
	// Give a clock to port D as we'll be using one of its pins for transfer of data.
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;

	// USART2 is connected to PD5 and PD6. Configure for Alternate Function
	GPIOD->MODER &= ~GPIO_MODER_MODE5;
	GPIOD->MODER &= ~GPIO_MODER_MODE6;
	GPIOD->MODER |= GPIO_MODER_MODE5_1;
	GPIOD->MODER |= GPIO_MODER_MODE6_1;

	// USART2 is AF7 
	GPIOD->AFR[0] |= ((7 << 20) | (7 << 24));

	// Set PD5 and PD6 to high speed
	GPIOD->OSPEEDR |= ((3 << 10) | (3 << 12));
	
	// Configure for pull up
	GPIOD->PUPDR &= ~GPIO_PUPDR_PUPD5;
	GPIOD->PUPDR &= ~GPIO_PUPDR_PUPD6;
	GPIOD->PUPDR |= GPIO_PUPDR_PUPD5_0;
	GPIOD->PUPDR |= GPIO_PUPDR_PUPD6_0;
}

void UartDmaInit()
{
	// Enable a clock for DMA1
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;
	DMA1_Channel7->CCR &= ~DMA_CCR_EN;			// Disable DMA Channel
	DMA1_Channel7->CCR &= DMA_CCR_MEM2MEM;  // Disable memory to memory mode
	DMA1_Channel7->CCR &= DMA_CCR_PL;				// Reset Priority Level
	DMA1_Channel7->CCR |= DMA_CCR_PL_1;			// Set Priority Level to High
	DMA1_Channel7->CCR &= DMA_CCR_PSIZE;		// Peripheral data size = 8 bits
	DMA1_Channel7->CCR &= ~DMA_CCR_MSIZE;	  // Memory data sizeof = 8 bits
	DMA1_Channel7->CCR &= ~DMA_CCR_PINC;		// Disable peripheral increment mode
	DMA1_Channel7->CCR |= DMA_CCR_MINC;			// Enable memory increment mode
	DMA1_Channel7->CCR &= ~DMA_CCR_CIRC;		// Disable Circular Mode
  DMA1_Channel7->CCR |= DMA_CCR_DIR;			// Transfer Direction: to peripheral*
	
	// Setting bit 4 specifies memory-to-peripheral communication.
	// Setting bit 7 specifies to increment the memory pointer after each data transfer.  This
	// allows the DMA device to progressively step through the dmaBuffer array.
	//DMA1_Channel7->CCR |= ((1 << 1)|(1 << 4)|(1 << 7));

	// Enable interrupt for DMA1_Stream7
	NVIC_SetPriority(DMA1_Channel7_IRQn, 0);
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);
}

void UartInit()
{
	UartGpioInit();

	// Give a clock USART2.  See pg 117.
	RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
	RCC->CCIPR &= ~RCC_CCIPR_USART2SEL;
	RCC->CCIPR |= RCC_CCIPR_USART2SEL_1;
	

	USART2->CR1 &= ~USART_CR1_UE;									// Disable USART
	USART2->CR1 &= ~USART_CR1_M;									// Set Data length to 8 bits
	USART2->CR2 &= ~USART_CR2_STOP;								// Select 1 stop bit
	USART2->CR2 &= ~USART_CR1_PCE;								// Select no parity
	USART2->CR1 &= ~USART_CR1_OVER8;							// Select oversampling at 16 bits

	// Baud Rate = 16 MHz / 0x0684 = 9598 Hz
	USART2->BRR |= 0x0683;

	// Enable USART2 for transmitting data.
	USART2->CR1 |= USART_CR1_TE; 									// Enable USART2 for transmission
	USART2->CR1 |= USART_CR1_UE;									// Enable USART2
	
  while ((USART2->ISR & USART_ISR_TEACK) == 0);	// Wait until USART is ready for transmission
	
	USART2->CR1 &= ~USART_CR1_TXEIE;							// Enable interrupts for transmitting
	//USART2->CR1 |= USART_CR1_TCIE;
	NVIC_SetPriority(USART2_IRQn, 1);
	NVIC_EnableIRQ(USART2_IRQn);
	
  //UartDmaInit();
	USART2->ICR |= USART_ICR_TCCF;
	//uartBusy = 0;
}

void SendString(char* string)
{
	// Copy the string into the DMA buffer and also calculate its length.
	int lengthOfString = 0;
	while(string[lengthOfString] != 0)
	{
		//dmaBuffer[lengthOfString] = string[lengthOfString];
		++lengthOfString;
	}
	
	for (int i = 0; i < lengthOfString; i++)
	{
		while(!(USART2->ISR & USART_ISR_TXE));
		USART2->TDR = string[i];
	}
	// Enable interrupt to occur upon completion of transfer
	//DMA1_Channel7->CCR |= (1 << 1);
	//DMA1_Channel7->CCR |= DMA_CCR_TCIE;

	// See pg 192.  This register holds the number of data items to transfer.
	//DMA1_Channel7->CNDTR = lengthOfString;

	// See pg 193.  This register holds the peripheral data register.  Since we're
	// using USART2 we set it to its data register.
	//DMA1_Channel7->CPAR = (uint32_t)&USART2->TDR;

	// See pg 193.  This register holds the memory address of the data we want to transfer.
	//DMA1_Channel7->CMAR = (uint32_t)&string;
	
	// Select DMA1 Channel 7 for USART2 TX
	//DMA1_CSELR->CSELR |= (2 << 24);

	// Enable the stream
	//DMA1_Channel7->CCR |= 1;

	// Clear the transfer complete flag in the UART SR
	//USART2->ICR |= (1 << 6);

	// Enable DMA transmission
	//USART2->CR3 |= (1 << 7);
	while (!(USART2->ISR & USART_ISR_TC));
	USART2->ICR |= USART_ICR_TCCF;
	//uartBusy = 0;
}

char* IntegerToString(int value, char *result, int base)
{
    // check that the base is valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

void DisplayHeader()
{
	SendString("     Gyro\r\n");
}

void DisplayIntegerValue(short value)
{
	// Convert the number to string
	char valueAsString[12];
	IntegerToString(value, valueAsString, BASE_10);

	// Right justify display the string
	short strlen = 0;
	while(valueAsString[strlen])
	{
		strlen++;
	}

	while(strlen < 7)
	{
		SendString(" ");
		++strlen;
	}

	SendString(valueAsString);
}

void DisplayAxisValue(char* label, short accel, short gyro)
{
	SendString(label);
	SendString(":");
	DisplayIntegerValue(gyro); 
}

void DisplayAxisValues()
{
	short accelX = 0, accelY = 0, accelZ = 0;
	//GetAccelerometerValues(&accelX, &accelY, &accelZ);

	short gyroX, gyroY, gyroZ;
	GetGyroValues(&gyroX, &gyroY, &gyroZ);

	DisplayAxisValue("X", accelX, gyroX);
	SendString("\r\n");
	DisplayAxisValue("Y", accelY, gyroY);
	SendString("\r\n");
	DisplayAxisValue("Z", accelZ, gyroZ);

	SendString("\033[2A");  // Makes cursor go up two lines
	SendString("\r");       // Return cursor to the beginning of the line
}