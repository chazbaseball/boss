#include "stm32l476xx.h"
#include "UART.h"
#include "driver_config.h"

void DMA1_Channel7_IRQHandler(void)
{
	// See pg 187.  Bit 21 is the "Stream 7 transfer complete interrupt flag".  This bit
	// will be set when the DMA transfer is complete.
	if(((DMA1->ISR) & (1 << 25)) != 0)
	{
		// See pg 188.  Here we clear the transfer complete interrupt.
		DMA1->IFCR |= (1 << 25);

		// See pg 552.  Here we specify that we want an interrupt generated once the
		// USART transmission is complete.
		USART2->CR1 |= (1 << 6);
	}
}

void USART2_IRQHandler(void)
{
	// See pg 549.  Bit 6 of the status register will be set when the UART
	// transmission has completed.
	if(((USART2->ISR) & (1 << 6)) != 0)
	{
		// Clear the interrupt. (...So that it doesn't continually trigger)
		USART2->CR1 &= ~(1 << 6);

		// Clear the busy flag to allow the next transmission.
		uartBusy = 0;
	}
}

void UartGpioInit()
{
	// Give a clock to port A as we'll be using one of its pins for transfer of data.
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;

	// See pg 19 of the ST UM1842 document.  We'll be using USART2.  USART2 TX occurs on
	// PA2 and USART2 RX occurs on PA3 so we set this pin to alternate mode.
	GPIOA->MODER |= ((1 << 5) | (1 << 7));

	// See pg 149 of ST RM0383 document.  USART2 is AF7.  And pg 160 of the same document
	// shows alternate function for pins 2 and 3 are set using alternate function low register
	// bits 8-thru-11.
	GPIOA->AFR[0] |= ((7 << 8) | (7 << 12));

	// Set PA2 and PA3 to high speed
	GPIOA->OSPEEDR |= ((3 << 4) | (3 << 6));
}

void UartDmaInit()
{
	// Enable a clock for DMA1
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;

	// Select DMA1 Channel 7 for USART2 TX
	DMA1_CSELR->CSELR |= (2 << 24);
	
	// See pg 189 for details of the DMA stream configuration register
	// Setting bit 4 specifies memory-to-peripheral communication.
	// Setting bit 7 specifies to increment the memory pointer after each data transfer.  This
	// allows the DMA device to progressively step through the dmaBuffer array.
	DMA1_Channel7->CCR |= ((1 << 1)|(1 << 4)|(1 << 7));

	// Enable interrupt for DMA1_Stream7
	NVIC_SetPriority(DMA1_Channel7_IRQn, 0);
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);
}

void UartInit()
{
	UartGpioInit();

	UartDmaInit();

	// Give a clock USART2.  See pg 117.
	RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
	RCC->CCIPR &= RCC_CCIPR_USART2SEL;
	RCC->CCIPR |= RCC_CCIPR_USART2SEL_0;

	// Here we set the baud rate.  This is explained on 519 of ST RM0383.  The peripheral
	// clock is 16 MHz by default.  So, the calculation for the USARTDIV is:
	// DesiredBaudRate = 16MHz / 16*USARTDIV
	// Note that the fractional part of USARTDIV is represented with only 4 bits.  So
	// if we use 9600 this will result in a small error of 0.02% (see pg 522).  Therefore
	// the baud rate is actually 9,598.  Again, see pg 522.
	USART2->BRR |= 0x683;

	// Enable USART2 for transmitting data.
	USART2->CR1 |= ((1 << 3) | (1 << 13));

	// See pg 200 of RM0383 ("STM32F411xC/E Reference Manual") for the interrupt
	// vector table.  You'll see TIM3 is interrupt 29.
	// See pg 209 of PM0214 ("STM32F4 Series Programming Manual") for info on "ISER"
	// which is one of the "interrupt set-enable registers".  We enable interrupt
	// number 29.
	NVIC_SetPriority(USART2_IRQn, 1);
	NVIC_EnableIRQ(USART2_IRQn);
	
	uartBusy = 0;
}

void SendString(char* string)
{
	// Here we block until the previous transfer completes.
	while(uartBusy == 1);
	uartBusy = 1;

	// Copy the string into the DMA buffer and also calculate its length.
	int lengthOfString = 0;
	while(lengthOfString < MAX_BUFFER_SIZE && string[lengthOfString])
	{
		dmaBuffer[lengthOfString] = string[lengthOfString];
		++lengthOfString;
	}

	// See pg 192.  This register holds the number of data items to transfer.
	DMA1_Channel7->CNDTR = lengthOfString;

	// See pg 193.  This register holds the peripheral data register.  Since we're
	// using USART2 we set it to its data register.
	DMA1_Channel7->CPAR = (uint32_t)&USART2->TDR;

	// See pg 193.  This register holds the memory address of the data we want to transfer.
	DMA1_Channel7->CMAR = (uint32_t)&dmaBuffer;

	// Enable interrupt to occur upon completion of transfer
	DMA1_Channel7->CCR |= (1 << 1);

	// Enable the stream
	DMA1_Channel7->CCR |= 1;

	// Clear the transfer complete flag in the UART SR
	USART2->ICR &= (1 << 6);

	// Enable DMA transmission
	USART2->CR3 |= (1 << 7);
}

char* IntegerToString(int value, char *result, int base)
{
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

void DisplayHeader()
{
	SendString("    Accel   Gyro\r\n");
}

void DisplayIntegerValue(short value)
{
	// Convert the number to string
	char valueAsString[12];
	IntegerToString(value, valueAsString, BASE_10);

	// Right justify display the string
	short strlen = 0;
	while(valueAsString[strlen])
	{
		strlen++;
	}

	while(strlen < 7)
	{
		SendString(" ");
		++strlen;
	}

	SendString(valueAsString);
}

void DisplayAxisValue(char* label, short accel, short gyro)
{
	SendString(label);
	SendString(":");
	//DisplayIntegerValue(accel); 
	DisplayIntegerValue(gyro); 
}

void DisplayAxisValues()
{
	short accelX, accelY, accelZ;
	//GetAccelerometerValues(&accelX, &accelY, &accelZ);

	short gyroX, gyroY, gyroZ;
	GetGyroValues(&gyroX, &gyroY, &gyroZ);

	DisplayAxisValue("X", accelX, gyroX);
	SendString("\r\n");
	DisplayAxisValue("Y", accelY, gyroY);
	SendString("\r\n");
	DisplayAxisValue("Z", accelZ, gyroZ);

	SendString("\033[2A");  // Makes cursor got up to lines
	SendString("\r");       // Return cursor to the beginning of the line
}